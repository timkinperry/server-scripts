docker volume create portainer_data
docker run -d --name portainer --restart always -p 8000:8000 -p 80:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
