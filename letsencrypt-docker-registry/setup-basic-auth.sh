#! /bin/bash

echo "Enter password for user (tim): "
read basic_auth_password
docker run --entrypoint htpasswd registry:2 -Bbn tim "$basic_auth_password" > /home/tim/docker_volumes/registry/auth/htpasswd
