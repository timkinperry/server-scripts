#! /bin/bash

docker run -it --rm -v /home/tim/docker_volumes/letsencrypt:/etc/letsencrypt certbot/certbot renew --email tim.r.perry@gmail.com --force-renew
