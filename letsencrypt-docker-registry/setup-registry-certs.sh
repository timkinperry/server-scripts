#! /bin/bash

echo -e "Going to try and get a certificate. For this to work port 443 needs to be forwarded me this machine"
docker run -it --rm -p 443:443 -v /home/tim/docker_volumes/letsencrypt:/etc/letsencrypt certbot/certbot certonly --keep-until-expiring --standalone --email tim.r.perry@gmail.com -d docker-registry.timperry.co.uk --agree-tos

pushd  /home/tim/docker_volumes/letsencrypt/live/docker-registry.timperry.co.uk/
cp privkey.pem domain.key
cat cert.pem chain.pem > domain.crt
chmod 777 domain.crt
chmod 777 domain.key
popd
