#! /bin/bash

renew_cert_script_path="$PWD/renew-registry-certs.sh"
renew_cert_cron_comment="DOCKER_REGISTRY_CERT_RENEW"
echo "Removing any old cronjobs"

sudo sed -i /$renew_cert_cron_comment/d /etc/crontab

echo "Setting up $renew_cert_script to be ran on the 1st of every month"
sudo echo "0 0 1 * * tim $renew_cert_script_path # $renew_cert_cron_comment " >> /etc/crontab

